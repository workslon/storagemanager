[TOC]

# Overview

NPM module - adapted version of [`sTORAGEmANAGER.js`](https://bitbucket.org/gwagner57/ontojs/src/badaa61994396729f101696f55b5249ef78e4c5c/src/sTORAGEmANAGER.js?at=master&fileviewer=file-view-default) written by Prof. Gerd Wagner, BTU Cottbus

You can find the source code [here](https://bitbucket.org/workslon/storagemanager/src/7b4214037736bdbca5413c03778c7222203e8af0/index.js)

The storage manager has a list of available storage adapters, one of which is assigned as the currently used adapter. Each available storage adapter (such as LocaStorage or MySQL in the diagram below) implements the CRUD methods specified in the abstract class AbstractStorageAdapter.

![sTORAGEmANAGER Diagram](assets/storage-manager-diagram.svg);

# Adapters

Currently the following adapters are supported:

+ [sTORAGEmANAGER_MySQL](https://bitbucket.org/workslon/storagemanager_mysql)

+ [sTORAGEmANAGER_ParseAPI](https://bitbucket.org/workslon/storagemanager_parseapi)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/storagemanager.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/storagemanager.git --save
```

# Usage

To start using the `sTORAGEmANAGER` module you just need to install it (see previous section) and configure it's adapter.

__NOTE:__ you don't need to load adapter - `sTORAGEmANAGER` is supplied already with all supported storage adaptors (see [`package.json`](https://bitbucket.org/workslon/storagemanager/src/6db44f67d302e24bacdea6554aea0d0934d0a9ad/package.json) for more details)

By default the storage manager uses LocalStorage adapter that use browser `LocalStorage` API. Default adapter can be changed providing the configuration object when initialize a new storage manager instance.

Every storage adapter has 5 main methods:

+ `retrieve()`

+ `retrieveAll()`

+ `add()`

+ `update()`

+ `destroy()`

Please see the examples of initialization of a new storage manager below (currently supported adaters considered).

## CommonJS

```javascript
var sTORAGEmANAGER = require('storage-manager');

// localStorage
var storageManager = new sTORAGEmANAGER();

// MySQL
var storageManager = new sTORAGEmANAGER({
  name: 'MySQL',
  pool: false
});

// Parse REST API
var mysqlStorageManager = new sTORAGEmANAGER({
  name: 'ParseRestAPI',
  appId: 'AppId',
  apiUrl: 'https://my-app.com/api/classes'
});
```

## ES6 Modules

```javascript
import sTORAGEmANAGER from 'storage-manager';
// localStorage
const storageManager = new sTORAGEmANAGER();

// MySQL
const storageManager = new sTORAGEmANAGER({
  name: 'MySQL',
  pool: false
});

// Parse REST API
const mysqlStorageManager = new sTORAGEmANAGER({
  name: 'ParseRestAPI',
  appId: 'AppId',
  apiUrl: 'https://my-app.com/api/classes'
});

```

# API References

API References for default LocalStorage Adapter

## Methods

__.retrieve(modelClass, id, continueProcessing)__ - Generic method for loading/retrieving a model object

| Name               | Type     | Description                         |
|--------------------|----------|-------------------------------------|
| modelClass         | Object   | mODELcLASS instance                 |
| id                 | String   | Id of the target object in database |
| continueProcessing | Function | Callback function                   |

__.retrieveAll(modelClass, continueProcessing)__ - Generic method for loading/retrieving all models

| Name               | Type     | Description                         |
|--------------------|----------|-------------------------------------|
| modelClass         | Object   | mODELcLASS instance                 |
| continueProcessing | Function | Callback function                   |

__.add(modelClass, slots, newObj, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| slots              | Object   | The slots for object creation                 |
| newObj             | Object   | The new object                                |
| continueProcessing | Function | Callback function                             |

__.update(modelClass, objectId, slots, newObj, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| objectId           | String   | ID of the object to be updated                |
| slots              | Object   | The slots for object creation                 |
| newObj             | Object   | The new object                                |
| continueProcessing | Function | Callback function                             |

__.destroy(modelClass, objectId, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| objectId           | String   | ID of the object to be destroyed              |
| continueProcessing | Function | Callback function                             |

__.clearData(modelClass, continueProcessing)__ - delete all entries

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| continueProcessing | Function | Callback function                             |