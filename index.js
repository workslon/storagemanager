/**
 * @fileOverview  This file contains the definition of the library class
 * sTORAGEmANAGER and its adapters.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
/**
 * Library class providing storage management methods for a number of predefined
 * storage adapters + storage adapters
 *
 * @constructor
 * @this {sTORAGEmANAGER}
 * @param storageAdapter: object
 */

var util = require('gw-util');
var errorTypes = require('error-types');
var ConstraintViolation = errorTypes.ConstraintViolation;

function sTORAGEmANAGER( storageAdapter) {
  if (!storageAdapter) this.adapter = {name:"LocalStorage"};
  else if (typeof( storageAdapter) === 'object' &&
      storageAdapter.name !== undefined &&
      ~["LocalStorage","ParseRestAPI","MySQL"].indexOf(storageAdapter.name)) {
    this.adapter = storageAdapter;
  } else {
    throw new ConstraintViolation("Invalid storageAdapter name");
  }
  // copy storage adapter to corresponding storage management method library
  sTORAGEmANAGER.adapters[this.adapter.name].currentAdapter = storageAdapter;
}
/**
 * Generic method for loading/retrieving a model object
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.retrieve = function (mc, id, continueProcessing) {
  var adapter = sTORAGEmANAGER.adapters[this.adapter.name];
  return adapter.retrieve.apply(adapter, arguments);
};
/**
 * Generic method for loading all table rows and converting them
 * to model objects
 *
 * @method
 * @param {object} mc  The model class concerned
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.retrieveAll = function (mc, continueProcessing) {
  mc.instances = {};  // clear main memory cache
  var adapter = sTORAGEmANAGER.adapters[this.adapter.name];
  return adapter.retrieveAll.apply(adapter, arguments);
};
/**
 * Generic method for creating and "persisting" new model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {object} slots  The object creation slots
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.add = function (mc, slots, continueProcessing) {
  var adapter = sTORAGEmANAGER.adapters[this.adapter.name];
  return adapter.add.apply(adapter, arguments);
};
/**
 * Generic method for updating model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {object} slots  The object's update slots
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.update = function (mc, id, slots, continueProcessing) {
  var adapter = sTORAGEmANAGER.adapters[this.adapter.name];
  return adapter.update.apply(adapter, arguments);
};
/**
 * Generic method for deleting model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.destroy = function (mc, id, continueProcessing) {
  var adapter = sTORAGEmANAGER.adapters[this.adapter.name];
  return adapter.destroy.apply(adapter, arguments);
};
/**
 * Generic method for clearing the database table, or object store,
 * of a model class
 * @method
 */
sTORAGEmANAGER.prototype.clearData = function (mc, continueProcessing) {
  if (confirm("Do you really want to delete all data?")) {
    var adapter = sTORAGEmANAGER.adapters[this.adapter.name];
    return adapter.clearData.apply(adapter, arguments);
  }
};

sTORAGEmANAGER.adapters = {};

/**************************************************************************
 * Storage management methods for the "LocalStorage" adapter
 **************************************************************************/
sTORAGEmANAGER.adapters["LocalStorage"] = {
  //------------------------------------------------
  retrieve: function (mc, id, continueProcessing) {
  //------------------------------------------------
    //this.retrieveAll();             //TODO: Needed?
    continueProcessing( mc.instances[id]);
  },
  //------------------------------------------------
  retrieveAll: function (modelClass, continueProcessing) {
  //------------------------------------------------
    function retrieveAll (mc) {
      var key = "", keys = [], i = 0,
          tableString = "", table={},
          tableName = util.getTableName( mc.name);
      Object.keys( mc.properties).forEach( function (p) {
        var range = mc.properties[p].range;
        if (range instanceof mODELcLASS) retrieveAll( range);
      });
      try {
        if (localStorage[tableName]) {
          tableString = localStorage[tableName];
        }
      } catch (e) {
        console.log( "Error when reading from Local Storage\n" + e);
      }
      if (tableString) {
        table = JSON.parse( tableString);
        keys = Object.keys( table);
        console.log( keys.length + " " + mc.name + " records loaded.");
        for (i=0; i < keys.length; i++) {
          key = keys[i];
          mc.instances[key] = mc.convertRec2Obj( table[key]);
        }
      }
    }
    retrieveAll( modelClass);
    continueProcessing();
  },
  //------------------------------------------------
  add: function (mc, slots, newObj, continueProcessing) {
  //------------------------------------------------
    mc.instances[newObj[mc.standardId]] = newObj;
    this.saveAll( mc);
    //console.log( newObj.toString() + " created!");
    continueProcessing( newObj, null);  // no error
  },
  //------------------------------------------------
  /***** newObj includes validated update slots *****/
  update: function (mc, id, slots, newObj, continueProcessing) {
  //------------------------------------------------
    this.saveAll( mc);  //TODO: save only on leaving page or closing browser tab/window
  },
  //------------------------------------------------
  destroy: function (mc, id, continueProcessing) {
  //------------------------------------------------
    delete mc.instances[id];
    this.saveAll( mc);  //TODO: save only on app exit
  },
  //------------------------------------------------
  clearData: function (mc, continueProcessing) {
    //------------------------------------------------
    var tableName = util.getTableName( mc.name);
    mc.instances = {};
    try {
      localStorage[tableName] = JSON.stringify({});
      console.log("Table "+ tableName +" cleared.");
    } catch (e) {
      console.log("Error when writing to Local Storage\n" + e);
    }
  },
  //------------------------------------------------
  saveAll: function (mc) {
  //------------------------------------------------
    var id="", table={}, rec={}, obj=null, i=0,
        keys = Object.keys( mc.instances),
        tableName = util.getTableName( mc.name);
    // convert to a 'table' as a map of entity records
    for (i=0; i < keys.length; i++) {
      id = keys[i];
      obj = mc.instances[id];
      table[id] = obj.toRecord();
    }
    try {
      localStorage[tableName] = JSON.stringify( table);
      console.log( keys.length +" "+ mc.name +" records saved.");
    } catch (e) {
      console.log("Error when writing to Local Storage\n" + e);
    }
  }
};

// -- Adapters
// MySQL
sTORAGEmANAGER.adapters['MySQL'] = require('storage-manager-mysql');
// Parse REST API
sTORAGEmANAGER.adapters['ParseRestAPI'] = require('storage-manager-parse-api');

// -- exports
module.exports = sTORAGEmANAGER;